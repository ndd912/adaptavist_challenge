/*
Author: Nick Delamora
Date: January/February 2018

Program Description: Calculates large factorials by storing digits in a vector

Arguments: An integer between 1 and 500 (Not sure of actual max limitation)

Proof: To check performance visit wolframalpha.com and type your number factorial
to compare digit results
*/
#include <vector>
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

void printBigNumber(vector<int> digits){
    for(int i = digits.size()-1; i > -1; i--){
        cout << digits[i];
    }
    cout << endl;
    cout << "Number is " << digits.size() << " digit(s)" << endl;
}

void printVector(vector<int> v){
    cout << '[';
    for(int i = 0; i < v.size(); i++){
        if(i == v.size()-1){
            cout << v[i] << ']' << endl;
        }
        else{
            cout << v[i] << ", ";
        }
    }
}

vector<int> normalizeArray(vector<int> digArr, bool debugFlag){
    //This algorithm can be generalized and made shorter but that was beyond the scope (only needed 200!)
    //Handles carries once numbers have been multiplied
    //Values like 24 are represented in opposite order [4,2]
    //Example 5*[4,2] = [20, 10]
    //Step 1: [0, 12] Add tens place to next element
    //Step 2: [0, 2, 1] Push tens place to back of vector since there is no next element
    vector<int> tempArray;
    int x;
    int numCarries;
    bool carries;
    bool processed;
    for(int i = 0; i < digArr.size(); i++){
        x = digArr[i];
        processed = false;
        if(x >= 10000 && !processed){
            if(i+1 == digArr.size()){
                digArr.push_back(((x-(x%10))/10)%10);
                digArr.push_back(((x-(x%100))/100)%10);
                digArr.push_back(((x-(x%1000))/1000)%10);
                digArr.push_back((x-(x%10000))/10000);
                digArr[i] = x%10;
            }
            else if(i+2 == digArr.size()){
                digArr.push_back(((x-(x%100))/100)%10);
                digArr.push_back(((x-(x%1000))/1000)%10);
                digArr.push_back((x-(x%10000))/10000);
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else if(i+3 == digArr.size()){
                digArr.push_back(((x-(x%1000))/1000)%10);
                digArr.push_back((x-(x%10000))/10000);
                digArr[i+2] += ((x-(x%100))/100)%10;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else if(1+4 == digArr.size()){
                digArr.push_back((x-(x%10000))/10000);
                digArr[i+3] += ((x-(x%1000))/1000)%10;
                digArr[i+2] += ((x-(x%100))/100)%10;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else{
                digArr[i+4] += (x-(x%10000))/10000;
                digArr[i+3] += ((x-(x%1000))/1000)%10;
                digArr[i+2] += ((x-(x%100))/100)%10;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            processed = true;
        }
        if(x >= 1000 && !processed){
            if(i+1 == digArr.size()){
                digArr.push_back(((x-(x%10))/10)%10);
                digArr.push_back(((x-(x%100))/100)%10);
                digArr.push_back((x-(x%1000))/1000);
                digArr[i] = x%10;
            }
            else if(i+2 == digArr.size()){
                digArr.push_back(((x-(x%100))/100)%10);
                digArr.push_back((x-(x%1000))/1000);
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else if(i+3 == digArr.size()){
                digArr.push_back((x-(x%1000))/1000);
                digArr[i+2] += ((x-(x%100))/100)%10;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else{
                digArr[i+3] += (x-(x%1000))/1000;
                digArr[i+2] += ((x-(x%100))/100)%10;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            processed = true;
        }
        if(x >= 100 && !processed){
            if(i+1 == digArr.size()){
                digArr.push_back(((x-(x%10))/10)%10);
                digArr.push_back((x-(x%100))/100);
                digArr[i] = x%10;
            }
            else if(i+2 == digArr.size()){
                digArr.push_back((x-(x%100))/100);
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            else{
                digArr[i+2] += (x-(x%100))/100;
                digArr[i+1] += ((x-(x%10))/10)%10;
                digArr[i] = x%10;
            }
            processed = true;
        }
        if(x >= 10 && !processed){
            if(i+1 == digArr.size()){
                digArr.push_back((x-(x%10))/10);
                digArr[i] = x%10;
            }
            else{
                digArr[i+1] += (x-(x%10))/10;
                digArr[i] = x%10;
            }
            processed = true;
        }
        if(debugFlag){
            printVector(digArr);
        }
    }
    return digArr;
}

vector<int> computeLargeFactorial(int x){
    vector<int> digitArray;
    int product;
    bool debugFlag = false;
    digitArray.push_back(1); //Start with 1
    for(int i = 2; i < x+1; i++){
        //multiply each element in the array by the current value of i
        for(int j = digitArray.size()-1; j > -1; j--){
            product = digitArray[j]*i;
            digitArray[j] = product;
        }
        //send to normalize function to handle carries
        digitArray = normalizeArray(digitArray, debugFlag);
    }
    return digitArray;
}


int main(){
    int numToCompute = 20; //Change this value to change output

    //Compute the factorial
    vector<int> digitsOfFactorial = computeLargeFactorial(numToCompute);
    printBigNumber(digitsOfFactorial);

    return 0;
}