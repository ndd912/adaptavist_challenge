## Repo Info

**Contributor:** Nick Delamora

**Date Created:** February 2018

**Purpose:** Adaptavist Challenege

---

## Contents

1. FizzBuzz.groovy - groovy script that executes the fizz buzz test (print integers from 1 to 100 [fizz on multiples of 3, buzz on multiples of 5, and fizzbuzz on multiples of 3 and 5])

    (**Note:** this was created in the groovy console tool in IntelliJIDEA)


2. LargeFactorial.cpp - c++ program that can compute the digits to large factorial numbers without using floating point representation

    **a)**You can run this by going to the online c++ compiler 'cpp.sh' and pasting the source code in there, then playing with the numToCompute parameter in the main function and clicking **Run**


    **b)**There is an upper limit, but for the scope of my problem I needed only 200!, so enter a number between 1 to 500 for best results. you can compare the real answer at wolframalpha.com